<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/getData', 'EventController@getData')->name('getData');
Route::get('/getData/{id}', 'EventController@getDetailEvent')->name('getDetailEvent');
Route::post('/addEvent', ['as' => 'postAddEvent', 'uses' => 'EventController@postAddEvent']);

// get title for autocomplade
Route::get('gettitle',[
	'as'=>'gettitle',
	'uses'=>'QuickEventController@gettitle'
]);
// post and edit event
Route::post('addquickevent',[
	'as'=>'postAddQuickEvent',
	'uses'=>'QuickEventController@postAddQuickEvent'
]);
Route::post('editquichevent/{id}',[
	'as'=>'postEditQuickevent',
	'uses'=>'QuickEventController@postEditQuickevent'
]);
//delete event
Route::get('delete-event/{id}',[
	'as'=>'deleteevent',
	'uses'=>'QuickEventController@deleteevent'
]);
//resize
Route::post('resize-event/{id}',[
	'as'=>'postResizeEvent',
	'uses'=>'QuickEventController@postResizeEvent'
]);
//drop
Route::post('drop-event/{id}',[
	'as'=>'postdropEvent',
	'uses'=>'QuickEventController@postdropEvent'
]);
//post history

Route::group(['prefix'=>'history'],function(){
	Route::post('posthistory-edit-event/{id}',[
		'as'=>'postHistory',
		'uses'=>'HistoryController@postHistory'
	]);
	Route::post('posthistory-resize-event/{id}',[
		'as'=>'postResize',
		'uses'=>'HistoryController@postResize'
	]);
	Route::post('posthistory-drop-event/{is}',[
		'as'=>'postDrop',
		'uses'=>'HistoryController@postDrop'
	]);
});
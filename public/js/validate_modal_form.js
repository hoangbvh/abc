 $(document).ready(function () {
     var rules = {
         patientName: {
             required: true
         }
     };
     var messages = {
         patientName: {
             required: "Please enter name"
         }
     };
     $("#createAppointmentForm").validate({
         rules: rules,
         messages: messages
     });

 });
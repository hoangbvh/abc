    function add_detail_event() {
        getdata();
    }
    function getdata() {
        $.get( "/getData", function( data ) {
            console.log(data);
            var status = data.status;
            var user = data.user;
            var html = "";
            var html_user = "";

            for (var i = 0; i < status.length; i++) {
                html = html + "<option value='" + status[i].id + "'>"+status[i].name+"</option>";
            }
            for (var i = 0; i < user.length; i++) {
                html_user = html_user + "<option value='" + user[i].id + "'>"+user[i].name+"</option>";
            }
            $('.select_status').html(html);
            $('.select_user').html(html_user);
            $('.select_manager').html(html_user);
        });
    }
    function show_update_event() {
        var id = $('#myModal #idEvent').val();
        $.get( "/getData/"+id, function( data ) {
        console.log(data);
        $( "input[name$='title']" ).val(data.title);
        $( "input[name$='datetime_from']" ).val(data.start);
        $( "input[name$='datetime_to']" ).val(data.end);
        if (data.allDay == 0) {
            $( "input[name$='allDay']" ).attr('checked',false);
        }else{
            $( "input[name$='allDay']" ).attr('checked',true);
        }
        $( "input[name$='noti']" ).val(data.noti);
        getdata();
        });
    }
    function saveEvent() {
        var csrf = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
        url: "http://localhost/FullCalendar/public/addEvent",
        type: 'POST',
        dataType: "JSON",
        data: {
            title:$('#eventTitle').val(),
            start: $('#date_from').val() + ' ' + $('#time_from').val(),
            end: $('#date_to').val() + ' ' + $('#time_to').val(),
            '_token': csrf
        },
        success: function (data, status)
        {

        },
        error: function (xhr, desc, err)
        {


        }
    });
    }
$(document).ready(function() { // document ready
    function show_message() {
        $('.message-box').fadeIn().delay(3000).fadeOut();
    }
	// onClick show modal detail event
    $('#show_detail_event').on('click', function(e) {
        e.preventDefault();
        $("#myModal").modal('hide');
        $("#showdetail").modal('show');
    });
    // Check allDay
    $('#allDay').change(function() {
        var checked = $('#allDay').is(":checked");
        if (checked) {
            $('#time_from').val("00:00");
            $('#time_to').val("00:00");
            $('.time_to').addClass('hide');
            $('.time_from').addClass('hide');
            $('#eventAllDay').val(checked)
        } else {
            $('.time_to').removeClass('hide');
            $('.time_from').removeClass('hide');
            $('#eventAllDay').val(checked)
        }
    });
    //Clear data when show modal
    $('#detailEvent').on('shown.bs.modal', function() {
        $('#eventTitle').val("");
        $('#date_from').val("");
        $('#time_from').val("");
        $('#date_to').val("");
        $('#time_to').val("");
        $('.time_to').removeClass('hide');
        $('.time_from').removeClass('hide');
        $("#allDay").prop("checked", false);
    });
    // onclick submit edit Event
    $('#saveEvent').on('click', function(e) {
        e.preventDefault();
        if ($('#eventAllDay').val() == 'true') {
            isAllday = true;
        } else {
            isAllday = false
        };
    saveEvent();
        var data = {
            title: $('#eventTitle').val(),
            start: $('#date_from').val() + ' ' + $('#time_from').val(),
            end: $('#date_to').val() + ' ' + $('#time_to').val(),
            allDay: isAllday
        };
        // doSubmit(data);
    });
    $("#add_event_form").submit(function(event) {
        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }
        var form = $(this);
        var csrf = $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
        url: "http://localhost/FullCalendar/public/addEvent",
        type: 'POST',
        dataType: "JSON",
        data: new FormData(this),
        contentType: false,
        cache:false,
        processData:false,
        success: function (data, status)
        {
            if (data.success) {
               var event = data.data;
            $("#calendar").fullCalendar('renderEvent', event,
            true);
            $("#detailEvent").modal('hide');
            $("#calendar").append('<div class="alert alert-success message-box">'+ data.message +'</div>');
            show_message();
            }
        },
        error: function (xhr, desc, err)
        {


        }
    });
      /* stop form from submitting normally */
       
      event.preventDefault();
  });
});
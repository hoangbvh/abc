$(document).ready(function() { // document ready
    $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',
        defaultDate: Date.now(),
        timezone: 'Asia/Ho_Chi_Minh',
        height: 750,
        columnHeaderFormat :'ddd D/M',
        editable: true,
        selectable: true,
        allDay: false,
        droppable: true,
        navLinks: true,
        eventColor: '#378006',
        weekNumbers: true,
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        // timeFormat: 'H(:mm)',
        weekNumbersWithinDays: true,
        eventLimit: true, // allow "more" link when too many events
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaDay,agendaTwoDay,agendaWeek,month,listWeek'
        },
        titleFormat: 'DD/MM/YYYY',
        views: {
            agendaTwoDay: {
                type: 'agenda',
                duration: { days: 4 },
                // views that are more than a day will NOT do this behavior by default
                // so, we need to explicitly enable it
                groupByResource: true
                //// uncomment this line to group by day FIRST with resources underneath
                //groupByDateAndResource: true
            }
        },
        //// uncomment this line to hide the all-day slot
        //allDaySlot: false,
        // resources: [
        //     { id: 'a', title: 'Room A' },
        //     { id: 'b', title: 'Room B', eventColor: 'green' },
        //     { id: 'c', title: 'Room C', eventColor: 'orange' },
        //     { id: 'd', title: 'Room D', eventColor: 'red' }
        // ],
        // events: [
        //     { id: '1',  start: '2018-04-06', end: '2018-04-08', title: 'event 1', allDay: true,color: 'red' },
        //     { id: '2',  start: '2018-04-07T09:00:00', end: '2018-04-07T14:00:00', title: 'event 2',color: 'red' },
        //     { id: '3',  start: '2018-04-07T12:00:00', end: '2018-04-08T06:00:00', title: 'event 3',color: 'blue' },
        //     { id: '4',  start: '2018-04-07T07:30:00', end: '2018-04-07T09:30:00', title: 'event 4',color: 'orange' },
        //     { id: '5',  start: '2018-04-07T10:00:00', end: '2018-04-07T15:00:00', title: 'event 5',color: 'green' }

        // ],

        events: function(start, end, timezone, callback) {
            $.ajax({
              url: "http://localhost/FullCalendar/public/getData",
              dataType: 'json',
              success: function(data) {
                var events = [];
                var colorr = data.status[0]['color'];
                             
                $.each(data.events, function( index, value ) {
                    events.push({
                    id: value.id,
                    title: value.title,
                    start: value.start,
                    end: value.end, // will be parsed
                    status_id : value.status_id,
                    allDay: value.allDay,
                    color: (value.status_id ==1 )? data.status[1]['color']:data.status[0]['color']
                  });
                });
                console.log(events);
                callback(events);
              }
            });
          },
          
        select: function(start, end) { /*function click space*/
            starttime = start.format('YYYY-MM-DD HH:mm:ss');
            endtime = end.format('YYYY-MM-DD HH:mm:ss');
            var mywhen = starttime + ' - ' + endtime;
            var allDay = !start.hasTime() && !end.hasTime();
                
            if (allDay==true) {
                starttime = start.format('YYYY-MM-DD');
                endtime = end.format('YYYY-MM-DD');
                console.log(start);
                $('#myModalallday #adpatientName').val("");
                $('#myModalallday #submitButtonallDay').attr('data-action', 'renderEvent');
                $('#myModalallday #addatetime_from').val(starttime);
                $('#myModalallday #addatetime_to').val(endtime);
                $('#myModalallday #adapptStartTime').val(start);
                $('#myModalallday #adapptEndTime').val(end);
                $('#myModalallday #adapptAllDay').val(allDay);
                $('#myModalallday #when').text(mywhen);
                $('#myModalallday').modal('show');
                $('#deleteEvent').addClass('hide');
            }
            if (allDay==false) {
               console.log(start);
                $('#myModal #patientName').val("");
                $('#myModal #submitButton').attr('data-action', 'renderEvent');
                $('#myModal #datetime_from').val(starttime);
                $('#myModal #datetime_to').val(endtime);
                $('#myModal #apptStartTime').val(start);
                $('#myModal #apptEndTime').val(end);
                $('#myModal #apptAllDay').val(allDay);
                $('#myModal #when').text(mywhen);
                $('#myModal').modal('show');
                $('#deleteEvent').addClass('hide');
            }
            
        },
        // dayClick: function(date,allDay ,jsEvent, view, resource) {
        // console.log(
        // jsEvent,
        // date.format(),
        // );
        // },
        eventClick: function(calEvent, jsEvent, view) { /*function when click event*/
            $('#deleteEvent').removeClass('hide');
             starttime = calEvent.start.format('YYYY-MM-DD HH:mm:ss');
             endtime = calEvent.end.format('YYYY-MM-DD HH:mm:ss');
            // var mywhen = starttime + ' - ' + endtime;
            var allDay = !calEvent.start.hasTime() && !calEvent.end.hasTime();
            if(allDay==true){
                starttime = calEvent.start.format('YYYY-MM-DD');
                 endtime = calEvent.end.format('YYYY-MM-DD');
            }
            var event_name = calEvent.title;
            var alday = $("input[name=apptAllDayedit]").val();
            $('#myModaledit #idEventedit').val(calEvent.id);
            $('#myModaledit #datetime_fromedit').val(starttime);
            $('#myModaledit #datetime_toedit').val(endtime);
            $('#myModaledit #apptStartTimeedit').val(calEvent.start);
            $('#myModaledit #apptEndTimeedit').val(calEvent.end);
            $('#myModaledit #apptAllDayedit').val(allDay);
           // $('#myModal #when').text(mywhen);
            $('#myModaledit #patientNameedit').val(event_name);
            $('#myModaledit #submitButtonedit').attr('data-action', 'updateEvent');
            $('#myModaledit').modal('show');

            console.log(calEvent);
            // change the border color just for fun
            // $(this).css('border-color', 'red');
            
            $("#submitButtonedit").on('click',function(e){
                e.preventDefault();
                var patientName_edit = $("input[name=patientName_edit]").val();
                var datetime_start_edit = $("input[name=datetime_start_edit]").val();
                var datetime_end_edit = $("input[name=datetime_end_edit]").val();
                var token_edit = $("input[name=tokenedit]").val();  
                var idEvent = $("input[name=idEvent_edit]").val();
                var allday = $("input[name=apptAllDayedit]").val();
                //check hoan thanh
                var t = new Date();
                var milisc = Date.parse(t); 
                //console.log(milisc);
                var milisc_event = Date.parse(datetime_start_edit);
                //console.log(milisc_event);
                var kq = milisc_event - milisc;
               // console.log(kq);
                $.ajax({
                    type: "POST",
                    
                    //data: "title="+ patientName + "&start=" + datetime_from + "&end=" + datetime_to + "&_token=" + token,
                    data:{
                        title_edit: patientName_edit,
                        start_edit: datetime_start_edit,
                        end_edit: datetime_end_edit,
                        allDay : allday,
                        _token: token_edit,
                        status_id: (kq >= 0)? 1 : 2

                    },
                    url: "http://localhost/FullCalendar/public/editquichevent/"+idEvent,
                    success: function(data){

                        $("#calendar").fullCalendar('refetchEvents');
                         //alert("OK");
                         console.log(data);
                        
                    }
                });
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: 'history/posthistory-edit-event/'+calEvent.id,
                    data:{
                        title_old: calEvent.title,
                        start_old: starttime,
                        end_old: endtime,
                        allDay: alday
                    },
                    success: function(data){
                        console.log('ok');
                    }
                });
           });
        },
        windowResize: function(view) {
            console.log('The calendar has adjusted to a window resize');
        },
        eventResize: function(event, delta, revertFunc) {
            var resize_timeend = event.end.format();
            //console.log(event);
            var t = new Date();
            var milisc = Date.parse(t); 
            var milisc_event = Date.parse(resize_timeend);
            var kq = milisc_event - milisc;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: 'resize-event/' + event.id,
                data: {
                    resize_time:resize_timeend,
                    status_id_resize: (kq >= 0)? 1 : 2
                },
                success: function(data){
                    console.log(event.title + " end is now " + event.end.format());
                     $("#calendar").fullCalendar('refetchEvents');
                }
            });
            var da =delta._milliseconds;
            var msec = Date.parse(resize_timeend);
            msec-=da;
            var d = new Date(msec);
            var resize_timeend_old = moment(d).format('YYYY-MM-DD HH:mm:ss');
           // console.log(resize_timeend_old);
            var resize_timestart = event.start.format();
            var title = event.title;
            //check status_old
            var t = new Date();
            var milisc = Date.parse(t); 
            var milisc_event = Date.parse(resize_timeend_old);
            var kq = milisc_event - milisc;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: 'history/posthistory-resize-event/' + event.id,
                data: {
                    title_old : title,
                    timestart_old : resize_timestart,
                    resize_time_old : resize_timeend_old,
                    status_id_resize_old: (kq >= 0)? 1 : 2
                },
                success: function(data){
                   console.log("time old "+resize_timeend_old);
                },
                error: function(){
                    revertFunc();
                }
            });
        },
        drop: function(date) {
            console.log("Dropped on " + date.format());

        },
        eventDrop: function(event, delta, revertFunc) { /*function drop event*/
            var start = event.start.format();
            var end = event.end.format();
            //check status_id
            var t = new Date();
            var milisc = Date.parse(t); 
            var milisc_event = Date.parse(end);
            var kq = milisc_event - milisc;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: 'drop-event/' + event.id,
                data: {
                    drop_start:start,
                    drop_end: end,
                    status_id_drop: (kq >= 0)? 1 : 2
                },
                success: function(data){
                     console.log(event.title + " was dropped on " + event.start.format());
                     $("#calendar").fullCalendar('refetchEvents');
                },
                error: function(){
                    revertFunc();
                }
            });
            var timestart_new = event.start.format();
            var msec1 = Date.parse(timestart_new);
            var timeend_new = event.end.format();
            var msec2 = Date.parse(timeend_new);
            msec1-=(delta._data['minutes']*60*1000 + delta._data['days']*24*60*60*1000 + delta._data['hours']*60*1000*60+ delta._data['months']*30*24*60*60*1000);
            msec2-=(delta._data['minutes']*60*1000 + delta._data['days']*24*60*60*1000 + delta._data['hours']*60*1000*60+ delta._data['months']*30*24*60*60*1000);
            var d1 = new Date(msec1);
            var start_old_drop = moment(d1).format('YYYY-MM-DD HH:mm:ss');
            var d2 = new Date(msec2);
            var end_old_drop = moment(d2).format('YYYY-MM-DD HH:mm:ss');
            var title = event.title;
            //check status_id
            var t = new Date();
            var milisc = Date.parse(t); 
            var milisc_event = Date.parse(timeend_new);
            var kq = milisc_event - milisc;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: 'history/posthistory-drop-event/' + event.id,
                data: {
                    title_drop_old : title,
                    timestart_drop_old : start_old_drop,
                    timeend_drop_old : end_old_drop,
                    status_id_old: (kq >= 0)? 1 : 2
                },
                success: function(data){
                   console.log("time old "+start_old_drop + " - "+start_old_drop);
                }
                
            });
        },
        navLinkWeekClick: function(weekStart, jsEvent) {
            console.log('week start', weekStart.format()); // weekStart is a moment
            console.log('coords', jsEvent.pageX, jsEvent.pageY);
        }
    });
    // onClick button save event
    $('#submitButton').on('click', function(e) {
        e.preventDefault();
        var data = {
            id: Math.floor(Math.random() * 100),
            title: $('#patientName').val(),
            start: $('#apptStartTime').val(),
            end: $('#apptEndTime').val(),
            allDay: ($('#apptAllDay').val() == "true"),
        };
        $("#myModal").modal('hide');
        console.log(data);
        //doSubmit(data);
    });
    //onclick button save edit event
    $('#submitButtonedit').on('click', function(e) {
        e.preventDefault();
        var data = {
            id: Math.floor(Math.random() * 100),
            title: $('#patientNameedit').val(),
            start: $('#datetime_fromedit').val(),
            end: $('#datetime_toedit').val(),
            allDay: ($('#apptAllDayedit').val() == "true"),
        };
        //doSubmitedit(data);
        console.log(data);
        $("#myModaledit").modal('hide');
    });
    //onclick button save all day event
    $('#submitButtonallDay').on('click', function(e) {
        e.preventDefault();
        var data = {
            id: Math.floor(Math.random() * 100),
            title: $('#adpatientName').val(),
            start: $('#adapptStartTime').val(),
            end: $('#adapptEndTime').val(),
            allDay: ($('#adapptAllDay').val() == "true"),
        };
        //doSubmitallday(data);
        console.log(data);
        $("#myModalallday").modal('hide');
    });
   //onclick delete event
    $('#delete').on('click', function(e) {
        var idEvent = $("#idEventedit").val();
        console.log(idEvent);   
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    if (idEvent) {
                        $("#calendar").fullCalendar('removeEvents', idEvent);
                        $("#myModaledit").modal('hide');
                        $.ajax({
                            type : 'get',
                            url : 'delete-event/' + idEvent,
                            success : function(){
                                console.log("da xoa event:"+events);
                            }
                        });
                    }
                }
            });
    });

    // Check allDay
    $('#allDay').change(function() {
        var checked = $('#allDay').is(":checked");
        if (checked) {
            $('#time_from').val("00:00");
            $('#time_to').val("00:00");
            $('.time_to').addClass('hide');
            $('.time_from').addClass('hide');
            $('#eventAllDay').val(checked)
        } else {
            $('.time_to').removeClass('hide');
            $('.time_from').removeClass('hide');
            $('#eventAllDay').val(checked)
        }
    });
    
    //Clear data when show modal
    $('#detailEvent').on('shown.bs.modal', function() {
        $('#eventTitle').val("");
        $('#date_from').val("");
        $('#time_from').val("");
        $('#date_to').val("");
        $('#time_to').val("");
        $('.time_to').removeClass('hide');
        $('.time_from').removeClass('hide');
        $("#allDay").prop("checked", false);
    });
    // datetimepicker

    $('.form_datetime').datetimepicker({
        //language:  'fr',
        format:"yyyy-mm-dd hh:ii:ss",
        weekStart: 1,
        todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
        showMeridian: 1
    });
    // datepicker
    $('.form_date').datetimepicker({
        language: 'en',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    // timepicker
    $('.form_time').datetimepicker({
        language: 'en',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
});

// AIzaSyBrFRMtjUMDgZhNalHwwDAwYXjFXuzbNaE
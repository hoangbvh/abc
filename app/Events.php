<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';

    protected $fillable = ['title','start','end','description','files', 'status_id','user_id','manager_id','allDay','dow','ranges','detail'];

    public $timestamps = true;

    public function comments()
    {
        return $this->hasMany('App\Comments');
    }

    public function history()
    {
        return $this->hasMany('App\History','event_id','id');
    }

    public function status()
    {
        return $this->hasOne('App\History');
    }

    public function getEvents()
    {
        return Events::get();
    }

    public function getDetail($id)
    {
        return Events::where('id', $id)->first();
    }
}

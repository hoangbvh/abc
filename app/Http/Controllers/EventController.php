<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\StatusEvent;
use App\User;
use App\Events;
use App\Http\Requests\AddEventRequest;
 
class EventController extends Controller
{
    public function __invoke(Request $request)
    {
        //
    }

    public function getData()
    {
        $modelStatus = new StatusEvent;
        $status = $modelStatus->getStatus();
        $modelUser = new User;
        $modelEvent = new Events;
        $events = $modelEvent->getEvents();
        $user = $modelUser->getUser();
        return response()->json([
            'status' => $status,
            'user' => $user,
            'events' =>$events
        ] 
        );
    }
    

    public function getDetailEvent($id)
    {
        $modelEvent = new Events;
        $event = $modelEvent->getDetail($id);
        return $event;
    }

    public function postAddEvent(AddEventRequest $request)
    {
        $event = new Events();
        // Check allDay
        if ($request->allDay == null || $request->allDay == "") {
            $allDay = false;
        }else{
            $allDay = true;
        }
        // check date_time
        $date_time_from = date('Y-m-d', strtotime($request->date_from)) . 'T'  . date('H:i:s', strtotime($request->time_from));
        $date_time_to = date('Y-m-d', strtotime($request->date_to)) . 'T'  . date('H:i:s', strtotime($request->time_to));
        // Check repeat
        if ($request->dow == 'norepeat') {
            $dow = '[]';
        }else{
            $date_time_from = date('H:i:s', strtotime($request->time_from));
            $date_time_to = date('H:i:s', strtotime($request->time_to));
            if ($request->dow == 'daily') {
                $dow = '[0,1,2,3,4,5,6]';
            }else{
                $dofw = date('w', strtotime($request->date_from));
                $dow = '['.$dofw.']';
            }
        }
        // Upload file
        if (!empty($request->file('files'))) {
            $randomString = str_random(10);
            $fileName = $randomString . '-' . $request->file('files')->getClientOriginalName();
            $event->files = $fileName;
            $request->file('files')->move('uploads/', $fileName);
        }else{
            $event->files = "";
        }
        $event->title = $request->title;
        $event->group_id = 1;
        $event->start = $date_time_from;
        $event->end = $date_time_to;
        $event->noti = $request->noti;
        $event->type_noti = $request->type_noti;
        $event->description = $request->description;
        $event->status_id = $request->status;
        $event->user_id = $request->select_user;
        $event->manager_id = $request->select_manager;
        $event->allDay = $allDay;
        $event->dow = $dow;
        $event->ranges = "";
        $event->detail = "";
        $event->save();
        return response()->json([
            'success'=>true,
            'message' =>'success',
            'data'=>$event
            ]);
    }

 
    
}

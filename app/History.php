<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';

    protected $fillable = ['event_id','user_id','title_old','start_old','end_old','description_old','status_id','dow','ranges'];

    public $timestamps = true;

    public function Event()
    {
    	return $this->hasOne('App\Events','event_id','id');
    }
}


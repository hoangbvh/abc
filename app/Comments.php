<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = ['event_id','user_id','content','status'];

    public $timestamps = true;

    public function Event()
    {
    	return $this->hasOne('App\Events');
    }

    public function ReComments()
    {
    	return $this->hasMany('App\ReComments');
    }
}

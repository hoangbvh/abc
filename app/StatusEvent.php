<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusEvent extends Model
{
    protected $table = 'status_event';

    protected $fillable = ['name','color','description'];

    public $timestamps = true;

    public function Event()
    {
    	return $this->hasOne('App\Events');
    }

    public function getStatus()
    {
    	return StatusEvent::get();
    }
}

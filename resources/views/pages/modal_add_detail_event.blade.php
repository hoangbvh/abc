<!-- Modal add or edit detail event -->
<div class="modal fade" id="detailEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action=""  id="add_event_form" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Event</h4>
                </div>
                <div class="modal-body" id="detail_event">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Event</label>
                        <input type="text" name="title" class="form-control" id="eventTitle" placeholder="Title">
                    </div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="exampleInputPassword1">From: </label>
                            <div class="input-group date form_date col-md-5" data-date="" data-date-format="d M yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="16" id="date_from" name="date_from" type="text" value="" >
                                <span class=" input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <div class="input-group date form_time time_from col-md-5" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
                                <input class="form-control" size="8" type="text" id="time_from" name="time_from" value="" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input2" value="" /><br/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">To: </label>
                            <div class="input-group date form_date col-md-5" data-date="" data-date-format="d M yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                <input class="form-control" size="16" type="text" id="date_to" name="date_to" value="" >
                                <span class=" input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            <div class="input-group date form_time time_to col-md-5" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
                                <input class="form-control" size="8" type="text" id="time_to" name="time_to" value="" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="customcheck">All day
                                <input type="hidden" id="eventAllDay">
                                <input name="allDay" id="allDay" type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group repeat">
                            <div class="form-group">
                                <label for="sel1">Lặp:</label>
                                <select name="dow" class="form-control" id="sel1">
                                    <option value="norepeat">Không lặp</option>
                                    <option value="daily">Mọi ngày</option>
                                    <option value="weekly">Ngày này hàng tuần</option>
                                    <option value="monthly">Ngày này hàng tháng</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-inline">
                        <label for="exampleInputPassword1">Thông báo: </label>
                        <div class="form-group">
                            <input value="30" name="noti" class="form-control" type="number">
                        </div>
                        <div class="form-group">
                            <select name="type_noti" class="form-control" id="sel1">
                                <option value="m">Phút</option>
                                <option value="h">Giờ</option>
                                <option value="w">Tuần</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-inline">
                        <div class="form-group">
                            <label for="exampleFormControlFile1">File/Image</label>
                            <input name="files" type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái: </label>
                            <select name="status" class="form-control select_status" id="select_status">
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-inline" id="wo_ma">
                        <div class="form-group">
                            <label for="sel1">Người thực hiện:</label>
                            <select name="select_user" class="form-control select_user" id="select_user">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Quản lí:</label>
                            <select name="select_manager" class="form-control select_manager" id="select_manager">
                                
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-inline">
                        <label for="exampleInputPassword1">Mô tả: </label>
                        <div class="form-group">
                            <textarea name="description" class="form-control " id="editor1"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input class="btn btn-primary" type="submit" name="submit" value="submit">
                </div>
            </form>
        </div>
    </div>
</div>
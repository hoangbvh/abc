<!-- Modal quick add or edit Event -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form">Create Event</h4>
            </div>
            <div class="modal-body">
                <div class="btn-show-detail-event"><button onclick="show_update_event()" type="button" class="btn btn-danger" data-toggle="modal" id="show_detail_event"><span class=""><i class="glyphicon glyphicon-pencil"></i></span></button></div>
                <form id="createAppointmentForm" class="form-horizontal">
                    <input type="hidden" name="token" id="token" value="{{csrf_token()}}">
                    <div class="control-group">
                        <label class="control-label" for="inputPatient" >Event:</label>
                        <div class="controls">
                            <input class="form-control" type="text" name="patientName" id="patientName" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]"  placeholder="Title..">
                            <input class="form-control" type="hidden" id="idEvent"/>
                            <input class="form-control" type="hidden" id="apptStartTime"/>
                            <input class="form-control" type="hidden" id="apptEndTime"/>
                            <input class="form-control" type="hidden" id="apptAllDay" name="apptAllDay" />
                        </div>
                    </div>
                    <div class="form-inline datetime">
                        <div class="form-group datetime_input">
                            <label for="exampleInputName2">From: </label>
                            <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input class="form-control" id="datetime_from" type="text" value="" name="datetime_start" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="form-group datetime_input">
                            <label for="exampleInputEmail2">To: </label>
                            <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input class="form-control" id="datetime_to" type="text" value="" name="datetime_end" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               
                <button type="submit" data-action="" class="btn btn-primary" id="submitButton">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="myModalallday" class="modal fade" role="dialog">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form">Create Event</h4>
            </div>
            <div class="modal-body">
                <div class="btn-show-detail-event"><button onclick="show_update_event()" type="button" class="btn btn-danger" data-toggle="modal" id="show_detail_event"><span class=""><i class="glyphicon glyphicon-pencil"></i></span></button></div>
                <form id="createAppointmentForm" class="form-horizontal">
                    <input type="hidden" name="adtoken" id="token" value="{{csrf_token()}}">
                    <div class="control-group">
                        <label class="control-label" for="inputPatient" >Event:</label>
                        <div class="controls">
                            <input class="form-control" type="text" name="adpatientName" id="adpatientName" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]"  placeholder="Title..">
                            <input class="form-control" type="hidden" id="adidEvent"/>
                            <input class="form-control" type="hidden" id="adapptStartTime"/>
                            <input class="form-control" type="hidden" id="adapptEndTime"/>
                            <input class="form-control" type="hidden" id="adapptAllDay" name="adapptAllDay" />
                        </div>
                    </div>
                    <div class="form-inline datetime">
                        <div class="form-group datetime_input">
                            <label for="exampleInputName2">From: </label>
                            <div class="input-group date form_date col-md-9" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                                <input class="form-control" id="addatetime_from" type="text" value="" name="addatetime_start" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="form-group datetime_input">
                            <label for="exampleInputEmail2">To: </label>
                            <div class="input-group date form_date col-md-10" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                                <input class="form-control" id="addatetime_to" type="text" value="" name="addatetime_end" >
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               
                <button type="submit" data-action="" class="btn btn-primary" id="submitButtonallDay">Save</button>
            </div>
        </div>
    </div>
</div>

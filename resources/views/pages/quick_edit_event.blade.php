<!-- Modal quick add or edit Event -->
<div id="myModaledit" class="modal fade" role="dialog">
    <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title title_form">Edit Event</h4>
            </div>
            <div class="modal-body">
                <div class="btn-show-detail-event"><button onclick="show_update_event()" type="button" class="btn btn-danger" data-toggle="modal" id="show_detail_event"><span class=""><i class="glyphicon glyphicon-pencil"></i></span></button></div>
                <form id="createAppointmentForm" class="form-horizontal">
                    <input type="hidden" name="tokenedit" id="token" value="{{csrf_token()}}">
                    <div class="control-group">
                        <label class="control-label" for="inputPatient" >Event:</label>
                        <div class="controls">
                            <input class="form-control" type="text" name="patientName_edit" id="patientNameedit" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                            <input class="form-control" type="" id="idEventedit" name="idEvent_edit" />
                            <input class="form-control" type="" id="apptStartTimeedit"/>
                            <input class="form-control" type="" id="apptEndTimeedit"/>
                            <input class="form-control" type="" id="apptAllDayedit" name="apptAllDayedit" />
                        </div>
                    </div>
                    <div class="form-inline datetime">
                        <div class="form-group datetime_input">
                            <label for="exampleInputName2">From: </label>
                            <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii" data-link-field="dtp_input1">
                                <input class="form-control" id="datetime_fromedit" type="text" value=""  name="datetime_start_edit">
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="form-group datetime_input">
                            <label for="exampleInputEmail2">To: </label>
                            <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii" data-link-field="dtp_input1">
                                <input class="form-control" id="datetime_toedit" type="text" value=""  name="datetime_end_edit">
                                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                    </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="delete">Delete</button>
                <button type="submit" data-action="" class="btn btn-primary"  id="submitButtonedit">Save</button>
            </div>
        </div>
    </div>
</div>

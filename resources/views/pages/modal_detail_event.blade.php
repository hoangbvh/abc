<!-- Modal add or edit detail event -->
<div class="modal fade" id="showdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Event</h4>
      </div>
      <div class="modal-body" id="detail_event">
        <form action="">
          <div class="form-group">
            <label for="exampleInputPassword1">Event</label>
            <input type="text" name="title" class="form-control" id="eventTitle" placeholder="Title">
          </div>
          <div class="form-inline">
            <div class="form-group">
              <label for="exampleInputPassword1">From: </label>
              <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii p" data-link-field="dtp_input1">
                <input class="form-control" name="datetime_from" id="datetime_from" type="text" value="" readonly >
                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
              
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">To: </label>
              <div class="input-group date form_datetime col-md-5" data-date="" data-date-format="dd mm yyyy - HH:ii p" data-link-field="dtp_input1">
                <input class="form-control" name="datetime_to" id="datetime_to" type="text" value="" readonly >
                <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
            </div>
          </div>
          <div class="form-inline">
            <div class="form-group">
              <label class="customcheck">All day
                <input type="hidden" id="eventAllDay">
                <input name="allDay" id="allDay" type="checkbox">
                <span class="checkmark"></span>
              </label>
            </div>
            <div class="form-group repeat">
              <div class="form-group">
                <label for="sel1">Lặp:</label>
                <select name="dow" class="form-control" id="sel1">
                  <option value="norepeat">Không lặp</option>
                  <option value="daily">Mọi ngày</option>
                  <option value="weekly">Ngày này hàng tuần</option>
                  <option value="monthly">Ngày này hàng tháng</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-inline">
            <label for="exampleInputPassword1">Thông báo: </label>
            <div class="form-group">
              <input value="30" name="noti" class="form-control" type="number">
            </div>
            <div class="form-group">
              <select name="type_noti" class="form-control" id="sel1">
                <option value="m">Phút</option>
                <option value="h">Giờ</option>
                <option value="w">Tuần</option>
              </select>
            </div>
          </div>
          <div class="form-inline">
            <div class="form-group">
              <label for="exampleFormControlFile1">File/Image</label>
              <input name="files" type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Trạng thái: </label>
              <select name="status" class="form-control select_status" id="select_status">
              </select>
            </div>
          </div>
          <div class="form-inline" id="wo_ma">
            <div class="form-group">
              <label for="sel1">Người thực hiện:</label>
              <select name="select_user" class="form-control select_user" id="select_user">
                
              </select>
            </div>
            <div class="form-group">
              <label for="sel1">Quản lí:</label>
              <select name="select_manager" class="form-control select_manager" id="select_manager">
                
              </select>
            </div>
          </div>
          <div class="form-inline">
            <label for="exampleInputPassword1">Mô tả: </label>
            <div class="form-group">
              <textarea name="description" class="form-control description" id="description"></textarea>
            </div>
          </div>
          <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Bình luận</a></li>
              <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">Lịch sử</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="comments">
                @include('pages.comments')
              </div>
              <div role="tabpanel" class="tab-pane" id="history">..2.</div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveEvent">Save</button>
      </div>
    </div>
  </div>
</div>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8' />
        <base href="{{ asset('') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="css/fullcalendar.min.css" rel="stylesheet">
        <link href='css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <link href='css/scheduler.min.css' rel='stylesheet' />
        <link href='css/bootstrap.min.css' rel='stylesheet' />
        <link href='css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
        <link href='css/mycss.css' rel='stylesheet' />
        <link href='css/search.css' rel='stylesheet' />
        
    </head>
    <body>
         @include('search.search_event')
        <div id='calendar'>

            <div class="btn-add-event"><button onclick="add_detail_event()" type="button" class="btn btn-danger" data-toggle="modal" data-target="#detailEvent"><span class=""><i class="glyphicon glyphicon-plus"></i></span></button></div>
        </div>
        
        @include('pages.modal_add_detail_event')
        @include('pages.modal_detail_event')
        @include('pages.quick_add_event')
        @include('pages.quick_edit_event')
        
        </body>
        <script src='js/moment.min.js'></script>
        <script src='js/jquery.min.js'></script>
        <script src='js/bootstrap.min.js'></script>
        <script src='js/fullcalendar.min.js'></script>
        <script src='js/scheduler.min.js'></script>
        <script src='js/bootstrap-datetimepicker.min.js'></script>
        <script src='js/sweetalert.min.js'></script>
        <script src='js/myjs.js'></script>
        <script src='js/event.js'></script>
        <script src='ckeditor/ckeditor.js'></script>
        <script src='js/posteventdb.js'></script>
        <script src="js/autocomplete.js"></script>
        <script src="js/autocomplete_array.js"></script>
       <!--  <script src="js/change-datetime.js"></script> -->
       <!--  <script src="js/validate_modal_form.js"></script> -->
        <script>
            autocomplete(document.getElementById("myInput"), events);
        </script>
        <!-- <script>
            $(document).ready(function(){
                $("#calendar").fullCalendar({
                    eventClick: function(calEvent,jsEvent, view){
                        console.log('ok');
                    }
                });
            });
        </script> -->
        <script>
CKEDITOR.replace('editor1',{
filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
});
CKEDITOR.replace('description',{
filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
});
</script>
    </html>